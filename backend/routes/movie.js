const { request, response } = require('express')
const db = require('../db')
const utils = require('../utils')
const express = require('express')
const router = express.Router()

router.post('/addStudent', (request, response) => {
    const { student_name, password, course , passing_year, prn_no, dob } = request.body
    
    const query = 'insert into student ( student_name, password, course , passing_year, prn_no, dob ) values(?,?,?,?,?,?)'
    db.pool.execute(query, [student_name, password, course , passing_year, prn_no, dob], (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.get('/displayStudent', (request, response) => {
    const query = 'select student_id, student_name, password, course , passing_year, prn_no, dob from student'
    db.pool.execute(query, (error, result) => {
        response.send(utils.createResult(error,result))
    })
})

router.put('/updateStudent/:student_id', (request, response) => {
    const { student_id } = request.params;
    const { student_name, password, course , passing_year, prn_no, dob } = request.body
    
    const query = 'update student set student_title = ?, student_release_date = ?, student_time = ?, director_name = ? where student_id = ?';

    db.pool.execute(query, [student_name, password, course , passing_year, prn_no, dob, student_id], (error, result) => {
        response.send(utils.createResult(error, result));
    })
})

router.delete("/deleteStudent/:student_id", (request, response) => {
    const { student_id } = request.params;
    const query =
      "delete from student where student_id = ?";
  
    db.pool.execute(query, [student_id], (error, result) => {
      response.send(utils.createResult(error, result));
    });
  });

module.exports = router
